var crudApp = angular.module( 'crudApp', [ 'ngRoute', 'ngResource', 'bookServices', 'controllers' ] );

crudApp.config( [ '$routeProvider' , 
    function( $routeProvider ) {
        $routeProvider.
	        when( '/' , {
	            templateUrl: 'assets/partials/list.html'
	        }).
	        when( '/book/new' , {
	            templateUrl: 'assets/partials/new.html'
	        }).
	        when( '/book/edit/:id' , {
	            templateUrl: 'assets/partials/edit.html'
	        }).
	        otherwise({
	            redirectTo: function( routeParams , currentPath ) {
	                return '/';
	            }
	        });
	}
]);