var bookServices = angular.module( 'bookServices' , [ 'ngResource' ] );

bookServices.factory( 'Book' , [ '$resource' , function( $resource ) {
	return $resource( '/AngularCRUD/books/:id', { id: '@id' } , { update: { method: 'PUT' } } );
} ]);