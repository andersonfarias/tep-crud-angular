var controllers = angular.module( 'controllers', [ 'bookServices' ] );

controllers.controller( 'ListController' , [ '$scope' , '$http', 'Book' , function ( $scope , $http , Book ) {
	$scope.book = null;
	$scope.books = Book.query();
	
	$scope.onDeleteBtnClickedHandler = function( book ){
		$scope.book = book;
		$( '#confirmation-modal' ).modal( 'show' );
	};
	
	$scope.deleteBook = function() {
		$scope.book.$delete( function(){
			$( '#confirmation-modal' ).modal( 'hide' );
			$scope.books = Book.query();
			$( '#deleted-modal' ).modal( 'show' );
		});
		$scope.book = null;
	};
}]);

controllers.controller( 'NewController' , [ '$scope' , '$http', 'Book' , function ( $scope , $http , Book ) {
	$scope.book = {};

	$scope.onSubmitFormBtnClickedHandler = function() {
		Book.save( $scope.book , function(){
			$scope.onBookCreatedHandler();
		} );
	}
	
	$scope.onBookCreatedHandler = function() {
		$scope.book = {};
		$( '#created-modal' ).modal( 'show' );
	}
}]);

controllers.controller( 'EditController' , [ '$scope' , '$http', '$routeParams', '$location', 'Book' , function ( $scope , $http , $routeParams , $location , Book ) {
	$scope.book = Book.get({ id: $routeParams.id});

	$scope.onSubmitFormBtnClickedHandler = function() {
		$scope.book.$update( function(){
			$scope.onBookUpdatedHandler();
		} );
	}
	
	$scope.onBookUpdatedHandler = function() {
		$scope.book = {};
		$( '#updated-modal' ).modal( 'show' );
	}
	
	$scope.a = function(){
		$('#updated-modal').on('hidden.bs.modal', function (e) {
			window.location = '/AngularCRUD'; 
		});
		$( '#updated-modal' ).modal( 'hide' );
	}
}]);