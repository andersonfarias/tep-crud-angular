package br.com.ifpi.ads.tep

import grails.rest.Resource;

@Resource(uri='/books', formats=['json'])
class Book {

	String title
	
	String author
	
	String description
	
	BigDecimal value
	
    static constraints = {
		title size: 1..40, blank: false, unique: true
		author blank: false
		value blank: false
    }
}