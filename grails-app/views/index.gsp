<!DOCTYPE html>
<html ng-app="crudApp" lang="en">
	<head>
		<meta charset="utf-8" />
	    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	    <title>CRUD com Angular</title>
	    <meta name="viewport" content="width=device-width, initial-scale=1"/>
	    <asset:link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
	    
	    <asset:stylesheet href="bootstrap/dist/css/bootstrap.min.css"/>
	    <asset:stylesheet href="bootstrap/dist/css/bootstrap-theme.min.css"/>
	    
	    <asset:javascript src="jquery/dist/jquery.min.js"/>
	    <asset:javascript src="bootstrap/dist/js/bootstrap.min.js"/>
	    <asset:javascript src="application.js"/>
	    
	    <style type="text/css">
	    	body > .container {
				padding: 60px 15px 0;
			}
	    </style>
	</head>
	<body>
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
          			<a class="navbar-brand" href="#/">CRUD Angular</a>
				</div>
			</div>
		</nav>
		<div class="container">
			<div ng-view></div>
		</div>
	</body>
</html>